Hello guys and welcome to my repository.
I have developed this application without using any database (internal or external). For that reason, I had to use a stub which can be found in my models as the AccountStub. I just felt I should provide that here to make testing easier. 
Account numbers are as follows:
2314323453,
3314323453,
4314323453,
5314323453,
6314323453

The accountPassword for all of them is "1234". Thank you.
